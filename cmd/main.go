package main

import (
	"runtime"

	"image"
	"image/color"
	"image/png"

	"fmt"
	"math"
	"os"
	"sync"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

type Vec3 [3]float32

// none of these methods have pointer receivers to allow chaining

func (v Vec3) add(w Vec3) Vec3 {
	return Vec3{v[0] + w[0], v[1] + w[1], v[2] + w[2]}
}

func (v Vec3) sub(w Vec3) Vec3 {
	return Vec3{v[0] - w[0], v[1] - w[1], v[2] - w[2]}
}

// not to be confused with the scalar (or dot) product
func (v Vec3) scalar_mul(x float32) Vec3 {
	return Vec3{v[0] * x, v[1] * x, v[2] * x}
}

func (v Vec3) scalar_div(x float32) Vec3 {
	return Vec3{v[0] / x, v[1] / x, v[2] / x}
}

func (v Vec3) dot(w Vec3) float32 {
	return v[0]*w[0] + v[1]*w[1] + v[2]*w[2]
}

func (v Vec3) mag() float32 {
	return float32(math.Sqrt(float64(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])))
}

func (v Vec3) normalize() Vec3 {
	mag := v.mag()
	return Vec3{v[0] / mag, v[1] / mag, v[2] / mag}
}

type Ray struct {
	Origin    Vec3
	Direction Vec3
}

type Sphere struct {
	Center Vec3
	Radius float32

	// TODO: add support for texture mapping
	Color color.NRGBA

	// Transformations: currently translations only
	Transformations [][]Vec3
	Frame           int
	FrameCount      int
}

// solveQuadratic takes the coefficients of a quadratic equation and returns
// a boolean that is true when there are real solutions. It also returns two
// float32s that represent the solutions. These are the same if there is only
// one real solution.
func solveQuadratic(a, b, c float32) (bool, float32, float32) {
	discriminant := b*b - 4*a*c

	if discriminant < 0 {
		return false, 0, 0
	} else if discriminant == 0 {
		solution := -0.5 * b / a // more machine friendly version of -b/2a
		return true, solution, solution
	} else {
		solution := -0.5 * b / a // more machine friendly version of -b/2a
		return true, solution - float32(math.Sqrt(float64(discriminant))), solution + float32(math.Sqrt(float64(discriminant)))
	}
}

// intersectSphere attempts to find the intersection between a ray and a sphere
//
// It returns a bool to signify whether there was an intersection, a float that
// is the distance of the intersection, and the color of the sphere intersected
func (r *Ray) intersectSphere(s *Sphere) (bool, float32, color.NRGBA) {
	L := r.Origin.sub(s.Center)
	a := r.Direction.dot(r.Direction)
	b := 2 * r.Direction.dot(L)
	c := L.dot(L) - s.Radius*s.Radius

	// t0 and t1 are the distances of intersection
	solution, t0, t1 := solveQuadratic(a, b, c)
	if !solution {
		return false, 0, color.NRGBA{255, 0, 0, 255}
	}

	if t0 < t1 && t0 > 0 {
		return true, t0, s.Color
	} else if t1 > t0 && t1 > 0 {
		return true, t1, s.Color
	}

	return false, 0, color.NRGBA{0, 0, 255, 255}
}

type World struct {
	Spheres []*Sphere
	Camera  *Ray

	// this is all stuff that probably shouldn't be in the world struct
	ImageHeight int
	ImageWidth  int
	Fov         float32

	mut *sync.Mutex
	wg  *sync.WaitGroup
}

func (w *World) TransformSphere(index int) {
	sphere := w.Spheres[index]

	if len(sphere.Transformations) == 0 || len(sphere.Transformations)-1 < sphere.Frame {
		return
	}

	transforms := sphere.Transformations[sphere.Frame]
	sphere.Frame++

	for _, amount := range transforms {
		w.Spheres[index].Center = w.Spheres[index].Center.add(amount)
	}
}

func (w *World) AddTransformation(index int, transform Vec3, length int) {
	sphere := w.Spheres[index]

	for sphere.FrameCount < length {
		sphere.Transformations = append(sphere.Transformations, []Vec3{})
		sphere.FrameCount++
	}

	// increment for a single frame
	inc_transform := transform.scalar_div(float32(length))

	for i := 0; i < length; i++ {
		sphere.Transformations[i] = append(sphere.Transformations[i], inc_transform)
	}
}

func (w *World) renderPixel(x, y int) color.NRGBA {
	// magic maths
	dirX := (2.0*(float32(x)+0.5)/float32(w.ImageWidth) - 1.0) * float32(math.Tan(float64(w.Fov/2.0))) * float32(w.ImageWidth) / float32(w.ImageHeight)
	dirY := -(2.0*(float32(y)+0.5)/float32(w.ImageHeight) - 1.0) * float32(math.Tan(float64(w.Fov/2.0)))
	dir := Vec3{dirX, dirY, w.Camera.Direction[2]}.normalize()
	camRay := Ray{w.Camera.Origin, dir}

	mindist := float32(math.MaxFloat32) // shortest distance
	c := color.NRGBA{255, 255, 255, 255}
	if dirY > 0 {
		c = color.NRGBA{51, 179, 204, 255}
	}

	for _, s := range w.Spheres {
		intersect, dist, sphereColor := camRay.intersectSphere(s)

		if intersect && dist < mindist {
			c = sphereColor
		}
	}

	return c
}

func (w *World) Render(frame *image.NRGBA) {
	for i := 0; i < len(w.Spheres); i++ {
		w.TransformSphere(i)
	}

	for i := 0; i < w.ImageWidth; i++ {
		w.wg.Add(1)
		go func(x int, wg *sync.WaitGroup) {
			for j := 0; j < w.ImageHeight; j++ {
				frame.SetNRGBA(x, j, w.renderPixel(x, j))
			}

			wg.Done()
		}(i, w.wg)
	}

	w.wg.Wait()
}

func main() {
	// UHD
	const ImageWidth = 3840
	const ImageHeight = 2160

	frame := image.NewNRGBA(image.Rect(0, 0, ImageWidth, ImageHeight))

	w := World{
		Spheres: []*Sphere{
			&Sphere{Vec3{10, 10, 10}, 3, color.NRGBA{0, 0, 0, 255}, [][]Vec3{}, 0, 0},
			&Sphere{Vec3{10, 20, 30}, 4, color.NRGBA{0, 0, 0, 255}, [][]Vec3{}, 0, 0},
			&Sphere{Vec3{-10, 20, 30}, 4, color.NRGBA{0, 0, 0, 255}, [][]Vec3{}, 0, 0},
			&Sphere{Vec3{10, -20, 30}, 4, color.NRGBA{0, 0, 0, 255}, [][]Vec3{}, 0, 0},
			&Sphere{Vec3{-15, 20, 30}, 4, color.NRGBA{0, 0, 0, 255}, [][]Vec3{}, 0, 0},
		},
		Camera:      &Ray{Vec3{0, 0, 0}, Vec3{0, 0, 1}},
		ImageHeight: ImageHeight,
		ImageWidth:  ImageWidth,
		Fov:         math.Pi / 2.0,
		mut:         &sync.Mutex{},
		wg:          &sync.WaitGroup{},
	}

	w.AddTransformation(0, Vec3{10, 10, 10}, 60)
	w.AddTransformation(1, Vec3{-10, -10, -10}, 120)
	w.AddTransformation(2, Vec3{-10, -20, 10}, 240)

	/*
		var wg sync.WaitGroup

		for i := 0; i < 250; i++ {
			wg.Add(1)
			go func(j int, waitgroup *sync.WaitGroup) {
				w.Render(frame)
				fmt.Printf("%d frames rendered\n", j)
				time.Sleep(1 * time.Second)
				waitgroup.Done()
			}(i, &wg)

			time.Sleep(1 * time.Millisecond)
		}

		wg.Wait()

		f, err := os.Create("tmp.png")
		if err != nil {
			panic(err)
		}

		err = png.Encode(f, frame)
		if err != nil {
			panic(err)
		}
	*/

	for i := 0; i < 300; i++ {
		w.mut.Lock()

		w.Render(frame)

		if i%30 == 0 {
			fmt.Printf("%d frames rendered\n", i)
		}

		f, err := os.Create(fmt.Sprintf("frames/tmp_%04d.png", i))
		if err != nil {
			panic(err)
		}

		err = png.Encode(f, frame)
		if err != nil {
			panic(err)
		}

		w.mut.Unlock()
	}
}
